# DEMO PROJECT - SPRING BOOT
Ejemplo de proyecto spring boot con las siguientes tecnologias:

* Liquibase
* Spring Boot 2.3.3.RELEASE
* Spring security with jwt
* JPA with hibernate - spring repository
* Spring doc - open api
* TDD - junit and mock
* Java versión 8
* Lombok

## Sumario
* [Getting Started](#getting-Started)
* [Estructura de los modulos](#estructura-de-los-modulos)
* [Perfiles del proyecto](#perfiles-del-proyecto)
* [Acceso a base de datos H2](#acceso-a-base-de-datos-h2)
* [Swagger - Open Api, documentación del proyecto](#documentación-del-proyecto)

## Getting Started

### 1. Definiendo perfil
Los perfiles son definiciones de variables para poder
trabajar con spring boot y con un entorno personalizado.
El proyecto trabaja con 3:
 * **dev** Se usa para entornos de desarrollo.
 * **it**  Se usa durante las fases de pruebas de it.
 * **prod**  Se usa durante las fase de despliegue de producción.
 * **default** (No se asigna nada, es vacio, el proyecto interpreta como el default o prod)

Tenemos 2 formar de asignar un perfil para el entorno de desarrollo de la app

#### Forma 01: Variable de entorno
Asignar en la variable de entorno, la clave sera:
spring.profiles.active
```bash
#ejemplo
spring.profiles.active =  dev
```

![Minion](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/variables_de_entorno.png)

#### Forma 02: STS (IDE SPRING)
En la configuración del run tambien se puede establecer el perfil.

![Minion](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/sts.png)


### 2. Iniciando el proyecto

-  **Modo Consola:** Situarse en la raiz del proyecto demo-proj y ejecutar el siguiente comando.

```bash
mvn spring-boot:run
```

- **Modo ide STS:** Correr la clase principal

![Minion](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/run_main.png)


## Estructura de los modulos
```
demo-proj/
├── demo-controller
├── demo-dom
├── demo-dto
├── demo-ear
├── demo-service
├── demo-repository
├── demo-sql
└── demo-util
```

### demo-controller
Modulo de desarrollo de la app.

### demo-dto
Modulo de dto del proyecto para respuestas rest.

### demo-dom
Modulo de dominios.

### demo-ear
Modulo del ear final, es el componente final para el despliegue.

### demo-service
Modulo de la capa de servicio

### demo-repository
Modulo de la capa de persistencia.

### demo-sql
Modulo de proyecto que contiene la configuración liquibase. Se puede ejecutar solo
este modulo, tambien puede ejecutarse integrado al modulo demo-controller.

### demo-util
Modulo del utilitarios para el proyecto.

## Perfiles del proyecto

Los archivos de perfiles se definen por la sintaxis que viene despues de la palabra "application-", un ejemplo seria: 
- **application-_it_** para integracion. 
- **application-_dev_** para desarrollo.
- **application-_prod_** para para producción.
- **application** por default.

### Perfil IT: application-it
Este perfil usa las configuraciones para las pruebas de integración.
Como base de datos, usa H2. Una base de datos en memoria para realizar las pruebas con el modelo de persistencia.
Ademas que se ejecuta automaticamente durante el ciclo de vida de maven.

![profile-it](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/properties-it.png)

#### Lanzando una prueba de integracion

![profile-it](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/prueba_it_example.png)

#### Resultado de una prueba de integración

Por cada clase de prueba de integración se levanta un contexto web y de base de datos.

![profile-it](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/resultado_it.png)

### Perfil DEV: application-dev
Este perfil usa las configuraciones para la fase de desarrollo.

![profile-it](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/properties-dev.png)

### Perfil PROD: application-prod
Este perfil usa las configuraciones para la fase del despliegue de producción.

![profile-it](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/properties-prod.png)

----

## Acceso a base de datos H2
Las credenciales para acceder a la base de datos en memoria en fase de it, son las siguientes:
 - url: ../demo/h2-console/
 - username: sa
 - password: sa

![Minion](https://gitlab.com/cristhian_java/demo-app-springboot/-/raw/develop/img/h2_console.png)

## Documentación del proyecto
El acceso para la documentación de los servicios web es:
- [/demo/swagger-ui.html](/demo/swagger-ui.html)
